import styled from "styled-components";

const Title = styled.h1`
  font-size: 2em;
`;

export default () => (
  <div>
    <Title>About Me</Title>
  </div>
);
