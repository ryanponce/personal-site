import Link from "next/link";
import styled from "styled-components";

const Container = styled.div`
  margin: 0;
  max-width: 640px;
`;

const Greeting = styled.p`
  font-size: 2em;
  font-weight: 600;
`;

const Intro = styled.p`
  font-size: 1.5em;
`;

export default () => (
  <Container>
    <Greeting>👋🏼 Hi, I'm Ryan!</Greeting>
    <Intro>
      I'm a front-end engineer living in Brooklyn, New York. I work on the
      engineering team at <a href="https://say.com">Say</a>. You can read more
      about me{" "}
      <Link href="/about">
        <a>here</a>
      </Link>{" "}
      or follow me on <a href="https://twitter.com/ryanponce">Twitter</a>.
    </Intro>
  </Container>
);
