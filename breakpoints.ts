// https://www.styled-components.com/docs/advanced#media-templates
import { css } from "styled-components";

const sizes = {
  DESKTOP: 992,
  TABLET: 768,
  PHONE: 576
};

export const media = Object.keys(sizes).reduce(
  (acc, label) => {
    acc[label] = (
      literals: TemplateStringsArray,
      ...placeholders: any[]
    ) => css`
      @media (max-width: ${sizes[label]}px) {
        ${css(literals, ...placeholders)};
      }
    `;
    return acc;
  },
  {} as Record<
    keyof typeof sizes,
    (l: TemplateStringsArray, ...p: any[]) => string
  >
);
