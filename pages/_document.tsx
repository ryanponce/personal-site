import Document, { Head, Main, NextScript } from "next/document";
import styled, { ServerStyleSheet, injectGlobal } from "styled-components";
import Footer from "../components/Footer";
import { reset } from "styled-reset";

const globalStyles = injectGlobal`
  ${reset}
`;

const Html = styled.html`
  font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Helvetica,
    Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol";
  -webkit-text-size-adjust: 100%;
`;

const Body = styled.body`
  color: #10161a;
  font-size: 16px;
  min-height: 100vh;
`;

export default class MyDocument extends Document {
  static getInitialProps({ renderPage }) {
    const sheet = new ServerStyleSheet();
    const page = renderPage(App => (props: any) =>
      sheet.collectStyles(<App {...props} />)
    );
    const styleTags = sheet.getStyleElement();
    return { ...page, styleTags };
  }

  public render() {
    globalStyles;

    return (
      <Html>
        <Head>
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>Ryan Ponce</title>
          {this.props.styleTags}
        </Head>
        <Body>
          <Main />
          <NextScript />
          <Footer />
        </Body>
      </Html>
    );
  }
}
